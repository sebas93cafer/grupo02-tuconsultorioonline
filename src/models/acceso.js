const mongoose = require('mongoose');

const accesoSchema = new mongoose.Schema({
    nivelAcceso: { type: String, required: true},
},{
    timestamps: true,
})

module.exports = mongoose.model('acceso', accesoSchema)