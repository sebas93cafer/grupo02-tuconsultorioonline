const mongoose = require('mongoose')

const consultorioSchema = new mongoose.Schema({
    nombre: String,
    apellido: String
})

module.exports = mongoose.model('consultorio', consultorioSchema)