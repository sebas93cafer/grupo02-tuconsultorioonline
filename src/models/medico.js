const mongoose = require('mongoose');

const medicoSchema = new mongoose.Schema({
    usuario: { type: mongoose.Types.ObjectId, ref:'usuario'},
    consultorio: { type: mongoose.Types.ObjectId, ref:'consultorio'},
    rms: { type: String, required: true},
    especialidad1: { type: String, required: true},
    especialidad2: String,
    cargo: { type: String, required: true},
    horario: { type: Array, required: false}
},{
    timestamps: true,
})

module.exports = mongoose.model('medico', medicoSchema)