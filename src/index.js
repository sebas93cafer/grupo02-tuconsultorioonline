require('dotenv').config();
require('./database');
const express = require('express');
const app = express();
const port = 9000;
const medicoRouter = require('./routers/medicoRouter');
const accesoRouter = require('./routers/accesoRouter');
const usuarioRouter = require('./routers/usuarioRouter');

//Utilidades
app.use(express.json());

//Rutas
app.use('/', medicoRouter);
app.use('/', accesoRouter);
app.use('/', usuarioRouter);

app.listen(port, (req, res) => {
    console.log('Server online');
})