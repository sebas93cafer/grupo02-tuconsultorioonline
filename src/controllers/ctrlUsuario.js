const ctrlUsuario ={};
const usuario = require('../models/usuario');

//Métodos del CRUD
//Create
ctrlUsuario.save = async (req, res) => {
    await usuario
    .create(req.body)
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

//Read
ctrlUsuario.list = async (req, res) => {
    await usuario
    .find(req.body)
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

//ReadbyId
ctrlUsuario.listById = async (req, res) => {
    let id = req.params.id
    await usuario
    .findById(id)
    .then(data => res.json(data))
    .catch(err => res.json(err))
}
//SearchByName
ctrlUsuario.search = async (req, res) => {
    let name = req.params.name
    await usuario
    .find({name : RegExp(name, 'i')})
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

//Update
ctrlUsuario.update = async (req, res) => {
    const{_id, ...body} = req.body;
    await usuario
    .updateOne({_id : _id}, { $set : body})
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

//Delete
ctrlUsuario.delete = async (req, res) => {
    let id = req.params.id
    await usuario
    .deleteOne({id:id})
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

//UsuarioAusuario
ctrlUsuario.usuario = async (req, res) => {
    await usuario
    .find().populate('usuario')
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

module.exports = ctrlUsuario;