const ctrlMedico ={};
const medico = require('../models/medico');
const consultorio = require('../models/consultorio');

//Métodos del CRUD
//Create
ctrlMedico.save = async (req, res) => {
    await medico
    .create(req.body)
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

//Read
ctrlMedico.list = async (req, res) => {
    await medico
    .find(req.body)
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

//ReadbyId
ctrlMedico.listById = async (req, res) => {
    let id = req.params.id
    await medico
    .findById(id)
    .then(data => res.json(data))
    .catch(err => res.json(err))
}
//SearchByName
ctrlMedico.search = async (req, res) => {
    let name = req.params.name
    await medico
    .find({name : RegExp(name, 'i')})
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

//Update
ctrlMedico.update = async (req, res) => {
    const{_id, ...body} = req.body;
    await medico
    .updateOne({_id : _id}, { $set : body})
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

//Delete
ctrlMedico.delete = async (req, res) => {
    let id = req.params.id
    await medico
    .deleteOne({id:id})
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

//UsuarioAMedico
ctrlMedico.usuario = async (req, res) => {
    await medico
    .find()
    .populate('consultorio')
    .populate('usuario')
    //.
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

module.exports = ctrlMedico;