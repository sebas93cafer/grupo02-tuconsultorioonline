const ctrlAcceso ={};
const acceso = require('../models/acceso');

//Métodos del CRUD
//Create
ctrlAcceso.save = async (req, res) => {
    await acceso
    .create(req.body)
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

//Read
ctrlAcceso.list = async (req, res) => {
    await acceso
    .find(req.body)
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

//ReadbyId
ctrlAcceso.listById = async (req, res) => {
    let id = req.params.id
    await acceso
    .findById(id)
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

module.exports = ctrlAcceso;