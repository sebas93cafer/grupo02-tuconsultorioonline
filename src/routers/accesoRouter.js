const express = require('express');
const ctrlAcceso = require('../controllers/ctrlAcceso');
const router = express.Router();
const ctrAcceso = require('../controllers/ctrlAcceso');

//Create
router.post('/acceso', ctrlAcceso.save);

//Read
router.get('/acceso', ctrlAcceso.list);

//ReadById
router.get('/acceso/:id', ctrlAcceso.listById);

module.exports = router;