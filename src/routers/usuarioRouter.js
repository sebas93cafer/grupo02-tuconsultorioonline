const express = require('express');
const router = express.Router();
const ctrlUsuario = require('../controllers/ctrlUsuario');

//Create
router.post('/usuario', ctrlUsuario.save);

//Read
router.get('/usuario', ctrlUsuario.list);

//ReadById
router.get('/usuario/id/:id', ctrlUsuario.listById);

//SearchByName
router.get('/usuario/name/:name', ctrlUsuario.search);

//Update
router.put('/usuario', ctrlUsuario.update);

//Delete
router.delete('/usuario/:id', ctrlUsuario.delete);

module.exports = router;