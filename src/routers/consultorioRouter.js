const express = require('express');
const router = express.Router();
const ctrlConsultorio = require('../controllers/ctrlConsultorio');

//Create
router.post('/consultorio', ctrlConsultorio.save);

//Read
router.get('/consultorio', ctrlConsultorio.list);

//ReadById
router.get('/consultorio/id/:id', ctrlConsultorio.listById);

//SearchByName
router.get('/consultorio/name/:name', ctrlConsultorio.search);

//Update
router.put('/consultorio', ctrlConsultorio.update);

//Delete
router.delete('/consultorio/:id', ctrlConsultorio.delete);

module.exports = router;