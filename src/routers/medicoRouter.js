const express = require('express');
const router = express.Router();
const ctrlMedico = require('../controllers/ctrlMedico');

//Create
router.post('/medico', ctrlMedico.save);

//Read
router.get('/medico', ctrlMedico.list);

//ReadById
router.get('/medico/id/:id', ctrlMedico.listById);

//SearchByName
router.get('/medico/name/:name', ctrlMedico.search);

//ReadUsuario
router.get('/medico/usuario', ctrlMedico.usuario);

//Update
router.put('/medico', ctrlMedico.update);

//Delete
router.delete('/medico/:id', ctrlMedico.delete);

module.exports = router;